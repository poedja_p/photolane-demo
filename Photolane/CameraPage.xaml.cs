﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using System.Windows.Media.Imaging;
using Photolane.Model;
using Photolane.ViewModel;
using System.IO;

namespace Photolane
{
    public partial class CameraPage : PhoneApplicationPage
    {
        PhotoChooserTask photoChooser;
        string fileName;
        BitmapImage bmp;
        Stream photoStream;
        CameraViewModel viewModel = new CameraViewModel();
//        CameraViewModelLocal viewModel = new CameraViewModelLocal();

        public CameraPage()
        {
            InitializeComponent();


            photoChooser = new PhotoChooserTask();
            photoChooser.ShowCamera = true;
            photoChooser.Completed += photoChooser_Completed;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            
            photoChooser = null;
            bmp = null;
        }
     

        void photoChooser_Completed(object sender, PhotoResult e)
        {
            //set preview
            bmp = new BitmapImage();
            fileName = e.OriginalFileName;
            bmp.SetSource(e.ChosenPhoto);
            ImagePreview.Source = bmp;

            //set to begin
            e.ChosenPhoto.Seek(0, SeekOrigin.Begin);
            photoStream = e.ChosenPhoto;
        }

        private async void OnSendClicked(object sender, EventArgs e)
        {
            
            
            
            //send data
            //add to local
            Photo photo = new Photo();
            photo.caption = TextBoxCaption.Text;
            photo.file = fileName;

           
            //add to cloud
            var result = await viewModel.AddPhoto(photo, photoStream);
            
            
            
            
            
            if (result)
            {
                App.MainViewModel.timelineItems.Insert(0, photo);
                NavigationService.GoBack();
            }
            else {
                MessageBox.Show("There is error");    
            }
        }

        private void OnImageTapped(object sender, System.Windows.Input.GestureEventArgs e)
        {
            photoChooser.Show();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Photolane
{
    public partial class DetailPage : PhoneApplicationPage
    {
        public DetailPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            object selectedPhoto = null;
            if (PhoneApplicationService.Current.State.TryGetValue("SELECTED_PHOTO", out selectedPhoto))
            {
                DataContext = selectedPhoto;
            }
        }
    }
}
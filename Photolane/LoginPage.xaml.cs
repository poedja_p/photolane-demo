﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Photolane.ViewModel;

namespace Photolane
{
    public partial class LoginPage : PhoneApplicationPage
    {
        AppHelper helper = new AppHelper();

        public LoginPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (e.NavigationMode != NavigationMode.Back)
            {
                if (helper.IsLogin())
                {
                    NavigationService.Navigate(new Uri("/MainPage.xaml?login=true", UriKind.Relative));
                }
            }
            
        }

        private async void OnLoginTapped(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var user = await helper.Authenticate();
            if (user != null)
            {
                App.CurrentUser = user;
                NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
            }
        }
    }
}
﻿using Microsoft.WindowsAzure.MobileServices;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Photolane.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Photolane.ViewModel
{
    public class CameraViewModelLocal : ModelBase
    {



        public async Task<bool> AddPhoto(Photo photo,Stream photoStream)
        {
            await InsertPhoto(photo);
            var result = await UploadPhoto(photo, photoStream);
            return result;
        }

        /// <summary>
        /// Add photo to azure mobile service table Photo
        /// </summary>
        /// <param name="photo"></param>
        public async Task<Photo> InsertPhoto(Photo photo)
        {
            photo.file = "http://limaapril.files.wordpress.com/2014/09/pm-trello_thumb.jpg";
            return photo;
        }

        /// <summary>
        /// upload photo to azure storage
        /// </summary>
        /// <param name="photo"></param>
        /// <returns></returns>
        public async Task<Boolean> UploadPhoto(Photo photo,Stream  photoStream)
        {

            return true;
        }


    }
}

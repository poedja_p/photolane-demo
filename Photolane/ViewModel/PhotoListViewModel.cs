﻿using Microsoft.WindowsAzure.MobileServices;
using Photolane.Model;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Photolane.ViewModel
{
    public class PhotoListViewModel : ModelBase
    {

        MobileServiceClient client = new MobileServiceClient(ConfigHelper.MobileServiceUrl, ConfigHelper.MobileServiceKey);

        private ObservableCollection<Photo> _timelineItems;
        public ObservableCollection<Photo> timelineItems
        {
            get
            {
                return _timelineItems;
            }
            set
            {
                _timelineItems = value;
                NotifyPropertyChanged("timelineItems");
            }
        }

        public async void Load()
        {
            var photoTable = client.GetTable<Photo>();
            var result = await photoTable.ToCollectionAsync();

            timelineItems = new ObservableCollection<Photo>(result.OrderByDescending(item=>item.photoTimestamp));

        }


    }
}

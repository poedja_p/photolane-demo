﻿using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Phone.Notification;

namespace Photolane.ViewModel
{
    public class AppHelper
    {
        MobileServiceClient client = new MobileServiceClient(ConfigHelper.MobileServiceUrl, ConfigHelper.MobileServiceKey);

        public bool IsLogin()
        {
            if (App.CurrentUser == null)
            {
                if (IsolatedStorageSettings.ApplicationSettings.Contains("USER_ID"))
                {
                    var userId = IsolatedStorageSettings.ApplicationSettings["USER_ID"].ToString();
                    var userToken = IsolatedStorageSettings.ApplicationSettings["USER_TOKEN"].ToString();

                    App.CurrentUser = new MobileServiceUser(userId);
                    App.CurrentUser.MobileServiceAuthenticationToken = userToken;

                    return true;
                }
                else
                    return false;
            }
            else
                return true;
        }

        public async Task<MobileServiceUser> Authenticate()
        {
          
            string message;
            try
            {
                var user = await  client.LoginAsync(MobileServiceAuthenticationProvider.Facebook);
                App.CurrentUser = user;
               
                //save to local
                if (IsolatedStorageSettings.ApplicationSettings.Contains("USER_ID"))
                    IsolatedStorageSettings.ApplicationSettings["USER_ID"] = user.UserId;
                else
                    IsolatedStorageSettings.ApplicationSettings.Add("USER_ID", user.UserId);

                if (IsolatedStorageSettings.ApplicationSettings.Contains("USER_TOKEN"))
                    IsolatedStorageSettings.ApplicationSettings["USER_TOKEN"] = user.MobileServiceAuthenticationToken;
                else
                    IsolatedStorageSettings.ApplicationSettings.Add("USER_TOKEN", user.MobileServiceAuthenticationToken);

                

                IsolatedStorageSettings.ApplicationSettings.Save();

                return user;
            }
            catch (InvalidOperationException)
            {
                return null;
            }
         }

        public async Task<string> GetAppName()
        {
            try
            {
                var result = await client.InvokeApiAsync<AppName>("getappname");
                return result.appName;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public class AppName
        {
            public string appName { get; set; }
        }

    }
}

﻿using Photolane.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Photolane.ViewModel
{
    public class PhotoListViewModelLocal : ModelBase
    {
       


        private ObservableCollection<Photo> _timelineItems;
        public ObservableCollection<Photo> timelineItems
        {
            get
            {
                return _timelineItems;
            }
            set
            {
                _timelineItems = value;
                NotifyPropertyChanged("timelineItems");
            }
        }

        public void Load()
        {



            if (timelineItems == null)
                timelineItems = new ObservableCollection<Photo>();

            timelineItems.Add(new Photo() { caption = "Ini bagus", file = "Assets/Photos/IMG_2454.jpg" });
            timelineItems.Add(new Photo() { caption = "Ini bagus 2", file = "Assets/Photos/IMG_2709.jpg" });
            timelineItems.Add(new Photo() { caption = "Ini bagus 3", file = "Assets/Photos/IMG_2750.jpg" });
            timelineItems.Add(new Photo() { caption = "Ini bagus", file = "Assets/Photos/IMG_2801.jpg" });
            timelineItems.Add(new Photo() { caption = "Ini bagus 2", file = "Assets/Photos/IMG_2822.jpg" });


        }


    }
}

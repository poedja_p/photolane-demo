﻿using Microsoft.WindowsAzure.MobileServices;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Photolane.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Photolane.ViewModel
{
    public class CameraViewModel : ModelBase
    {

        MobileServiceClient client = new MobileServiceClient(ConfigHelper.MobileServiceUrl, ConfigHelper.MobileServiceKey);


        public async Task<bool> AddPhoto(Photo photo,Stream photoStream)
        {
            //versi login
            client.CurrentUser = App.CurrentUser;
            photo.userId = client.CurrentUser.UserId;

            //versi direct
            await InsertPhoto(photo);
            var result = await UploadPhoto(photo, photoStream);

            return result;
        }

        /// <summary>
        /// Add photo to azure mobile service table Photo
        /// </summary>
        /// <param name="photo"></param>
        public async Task<Photo> InsertPhoto(Photo photo)
        { 
              var photoTable = client.GetTable<Photo>();
              try
              {

                  await photoTable.InsertAsync(photo);
                  return photo;
              }
              catch (Exception  ex)
              {
                  return photo;
              }
        }

        /// <summary>
        /// upload photo to azure storage
        /// </summary>
        /// <param name="photo"></param>
        /// <returns></returns>
        public async Task<Boolean> UploadPhoto(Photo photo,Stream  photoStream)
        {

            try
            {
                if (!string.IsNullOrEmpty(photo.sasQueryString))
                {
                    // Get the URI generated that contains the SAS 
                    // and extract the storage credentials.
                    StorageCredentials cred = new StorageCredentials(photo.sasQueryString);
                    var imageUri = new Uri(photo.file);

                    // Instantiate a Blob store container based on the info in the returned item.
                    CloudBlobContainer container = new CloudBlobContainer(
                        new Uri(string.Format("https://{0}/{1}",
                            imageUri.Host, photo.containerName)), cred);

                    // Upload the new image as a BLOB from the stream.
                    CloudBlockBlob blobFromSASCredential =
                        container.GetBlockBlobReference(photo.resourceName);
                    await blobFromSASCredential.UploadFromStreamAsync(photoStream);


                    return true;
                }
                else {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }


    }
}

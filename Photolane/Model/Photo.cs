﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Photolane.Model
{
    public class Photo : ModelBase
    {

        public Photo()
        {
            resourceName = Guid.NewGuid().ToString();
            containerName = "photolane";
            photoTimestamp = DateTime.Now;
        }

        public string id { get; set; }

        private string _file;
        public string file
        {
            get {
                return _file; 
            }
            set {
                _file = value;
                NotifyPropertyChanged("file");
            }
        }

        private string _caption;
        public string caption
        {
            get {
                return _caption;
            }
            set {
                _caption = value;
                NotifyPropertyChanged("caption");
            }
        }

        private string _userId;
        public string userId
        {
            get {
                return _userId;
            }
            set {
                _userId = value;
                NotifyPropertyChanged("userId");
            }
        }

        private DateTime _photoTimestamp;
        public DateTime photoTimestamp
        {
            get {
                return _photoTimestamp;
            }
            set {
                _photoTimestamp = value;
                NotifyPropertyChanged("photoTimestamp");
            }
        }

        //blob container name
        public string containerName { get; set; }


        //file name
        public string resourceName { get; set; }

        public string sasQueryString { get; set; }

    }
}

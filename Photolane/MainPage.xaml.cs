﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Photolane.Resources;
using Photolane.ViewModel;
using System.Windows.Media.Imaging;
using System.IO;
using System.IO.IsolatedStorage;
using Photolane.Model;

namespace Photolane
{
    public partial class MainPage : PhoneApplicationPage
    {
        PhotoListViewModel vm = new PhotoListViewModel();

        // Constructor
        public MainPage()
        {
            InitializeComponent();


            DataContext = vm;

           
            App.MainViewModel = vm;
        }

        
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {

            if (e.NavigationMode != NavigationMode.Back)
            {

                if (NavigationContext.QueryString.ContainsKey("login"))
                    NavigationService.RemoveBackEntry();

                vm.Load();

                
                this.Title = await new AppHelper().GetAppName(); 
            }
        }

        private void MainLongListSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (MainLongListSelector.SelectedItem == null)
                return;

            // Navigate to the new page
            PhoneApplicationService.Current.State["SELECTED_PHOTO"] = MainLongListSelector.SelectedItem;
            NavigationService.Navigate(new Uri("/DetailPage.xaml", UriKind.Relative));

            MainLongListSelector.SelectedItem = null;
        }

        private void OnCameraTapped(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/CameraPage.xaml", UriKind.Relative));
        }

      

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Photolane
{
    public class PhotoStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                string [] splitResult = (value.ToString().Split(':'));
                return "http://graph.facebook.com/"+ splitResult[1]+ "/picture";
            }
            else
            {
                return "http://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?f=y";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
